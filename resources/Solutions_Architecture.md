
### Background
In the Solution Architect's eyes the customers have business problems. As an architect they will needs to show how their business product solve those problems.

#### Goals
* **COMMUNICATE OUR VALUE**: Solution architects show customers how their tool work together with other technologies to solve their problem.
* **INCREASE OPPORTUNITY TO SELL**: While exploring how the business product can help the customer, this will increase the customer pipeline by communicating to new customers via marketing, sales enablement, and sales engineering content.
* **ENGAGE THE ECOSYSTEM**: Train partners to implement solutions architecture for customers that need implementation resources and/pr industry expertise.

### Definition
A **solution architecture** is a documented, demo’able integration of our platform with itself, external services, and customer data that results in an immediate and obvious added value.

Solution architectures involve a close partnership between service organization and business teams.
A solution architecture requires:

* **Why**: A narrative describing the problem and the market demand.
* **How**: An architecture diagram (or similar) that shows how the solution works.
* **Proof**: A demo’able reference implementation that shows that the problem has been solved.

Solution architectures are used in:
* Sales content
* Marketing content
* SI partner training materials
* Whitepapers
* Blogs
* Sales engineering proof-of-concept work
* Tiers

Solutions architectures fall into three tiers depending on the tools and platforms they integrate, and the overall complexity of the solution itself.

The steps to evaluate whether a tool is compatible is through these steps.
#### Process
| Step |	Description	 | Outcome |
|:-:|:-:|:-|
|0.| Listen |	Create an analysis with a solution architecture idea based on a customer need|
|1.| Document|	Write out a document on how this is going to be combined with original product and customer need|
|2.| Validate|	Validate the problem and solution architecture proposal with the  customers.|
|3.| Prototype |	Develop functional requirements for the solution architecture	Complete solutions architecture template, section Requirements and Timelines.|
|4.| Execute |	Build a prototype of the solution architecture and document it.	A demo'able solution architecture with a clear why, how, and proof documented in the Solution Architecture template, section Finished Product.

## Listen
* What are customers asking for?
* Why do they keep saying It would be nice if A could do X
* What technical questions are they having, repeatedly?

## Validate
The questions to answer when reviewing a solutions architecture template in the proposal phase are:

* Does the proposal solve a specific, well-defined, customer-valued problem?
* Which customers would use (or potentially use) this solution?
* Would your customers or your team value this solution architecture? Why or why not?

## Prototype
Once you've sold us on the problem statement, it's time to begin work.Gather your requirements adn be specific about what is actually required vs what is nice to have. Sketch out some prototypes and then stack rank everything against how well they address the core customer problem. Be brutally honest...we ain't got time for busy work.

Think about time and resoruce costs to prototype, how the prototype might be deployed by a customer, if it would require a solutions integrater partner, and any technology pre-requisites. Use your problem-solving process to drive requirements.


Here's the success criteria:

* It has to work and actually solve the problem (80% solution or bust)
* It has to have documentation and a narrative
* Test it
