https://www.youtube.com/watch?v=1lgsQ3PKz9M

Intro to Kubernetes Outline

- Software Architecture Trends
  - Monolith
  - Microservice
- Containers
  - What?
    - linux threads
  - Who?
    - Docker
      - Mac, Linux and Windows
    - rkt
      - Windows
  - When?
    - circa 2005
  - Why?
    - needed a way to distribute software regardless of
- Hardships with Containers
  - Scalability
  - Failure Recovery
  - Routing
  - Scheduling
  - Networking
- Intro to Container Orchestration
  - What?
    - manage and deploy a series of containers
  - Who?
    - Cloud Services
      - ECS
      - EKS
      - Google
      - MS Azure
    - Definition of “Kubernetes”
      - is an open source project that enable developers to automate the process of deploying, scaling and managing containerized applications.
      - Achieve Declarative Configuration
      - Written in Go
      - Project by Google
  - When?
    - circa 2015
  - Why?
    - open source
    - cloud agnostic
- Kubernetes Architecture
  - Master Node
    - Controller Manager
    - Scheduler
    - Etcd
    - API Server
  - Worker Node
    - Docker
    - Kubelet Service
    - Proxy Service
- Kiki Delivery Service Application (Example Case)
  - Service Object (Handles Routing)
    - Load Balancer
  - Deployment Object  && Pods Object(Handles Scaling)
      - Microservices
        - UI
        - Backend
  - Persistence Volume Claim Object
    - Database
  - ConfigMaps Object
    - Secrets
  - Other Objects
    - Daemon Sets
    - Pod Objects
- Demo Example Project
- Conclusion
  - Scalability
  - Reliability
  - Fault Tolerance
  - Maintain Good Performance
  - Stability

----------------------------------------------------------------------------------------------------------------------------

# The Problem
## Old Age of Deployments
- Action Items
  [ ] Talk to Yves about horror stories and how applications were deployed.

Discuss the old process of deploying applications and state the problems associated with it.

- Dependency errors across different OS and Hardware when releasing new application versions. [ FAULT TOLERANCE ]
- Cost a lot of money, where companies had to physically own a series of servers to handle large amount of requests. [ SCALABILITY ]


## New Age of Deployments

Docker was introduced as a solution for shipping applications in containers.  It help aid in the process of providing faster deployments because of its ability to be transportable, light weight and can be shipped anywhere. However developers began to realize that in order to support millions of requests and scale in a reasonable manner they would have to manage over a 1000 containers.  With that came issues of managing networking, resources, and health status of the containers.

Questions for managing many containers?
- Where should my container live? (Kubernetes Scheduler)
- How do my container talk to other containers? (Networking)
- How do my container talk to the world? (Routing)
- What happen when the container dies? (Recovery)

Running with docker alone you come across issues like the following:
- Complicated internal network communication for containers along with its dependencies on other containers [ NETWORK ]
- Risk unavailability due to a key service in application being down (upgrades or service change) [ FAULT TOLERANCE ]
- Hard to manage containers that timeout or die in the process of handling requests [ HIGH AVAILABILITY OR FAILURE RECOVERY ]
- Can’t manually add or delete containers base off requests so it affects the server managing the containers. [ PERFORMANCE ]


# The Solution
## Solution to New Age Deployments

*Kubernetes*
- is an open source project that enable developers to automate the process of deploying, scaling and managing containerized applications. It is a container orchestrator.
- Created by Google
- Open Source
- Cloud Agnostic
- supports container technologies such as
  - Docker
  - rkt
  - cri-o
  - kata
  - Virtlet


*Why Kubernetes?*
People like to use Kubernetes with Docker for these 3-5 reasons:

- Scalability
- Reliability or Failure Recovery
- Networking
- Fault Tolerance
- Performance

# Kubernetes architecture
(Display abstraction diagram of a kubernetes architecture)
- Master Node
  - Scheduler
  - Controller
  - Etcd
  - Api Server
- Work Node
  - Pods and Deployments
  - Services
  - ConfigMaps
  - Volumes
  -
## Scalability
Scalability is the capability of a system, network, or process to handle a growing amount of work, or its potential to be enlarged to accommodate that growth. The way how kubernetes achieves is base of declarative configuration with the use of pods or deplpoyments.

Describe Pods and Deployments(Multiple Pods) and the concept of desired state. Controller will monitor keeping the state of the pods and will always try to maintain it. It also listens to when traffic is low and it will scale back. This is what we would call horizontal scaling.


## High Availability and Failure Recovery
High Availability is the idea that your application is accessible anytime and anywhere. The Schedule is a component in a master node, which is responsible for deciding which worker node should run a given pod.  If a node goes down it is the Scheduler job to launch a new deployment or pod.


## Networking
IP Addresses of containers change and are not reliable. This leads to networking and communciation issues amongst pods.  A way that Kubernetes handles the problem is the use of the Service Object.  The "Service" is a group of deployments or pods. It acts as a load balancer. The Ingress controller routes traffic from outside and into the cluster.

## Fault Tolerance
Fault tolerance is the property that enables a system to continue operating properly in the event of the failure of (or one or more faults within) some of its components. A fault-tolerant design enables a system to continue its intended operation, possibly at a reduced level, rather than failing completely, when some part of the system fails.

A tool that works well with Kuberenetes is Helm. It the version control of deployments. Using this tool will help account for the transitions between deployment versions as well conduct rollbacks when changes dont go as planned.


## Performance
- Allows to monitor computer resources efficiently
- Exapnd horizontally base on traffic (horizontal scalling)
- Ability to add tools for monitoring

# Conclusion
Achieve the 99.999% for your application!
- Scalability
- Reliability
- Fault Tolerance
- Maintain Good Performance
- Stability


High Availability >> https://itnext.io/keep-you-kubernetes-cluster-balanced-the-secret-to-high-availability-17edf60d9cb7
Amys Talk >> https://www.youtube.com/watch?v=vQX5nokoqrQ
High Availability >> https://thenewstack.io/kubernetes-high-availability-no-single-point-of-failure/
Scale Reliability >> https://thenewstack.io/taking-closer-look-kubernetes-part-two/
