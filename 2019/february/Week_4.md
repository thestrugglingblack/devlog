#### 2019-02-19
- **Security Group** is a virtual firewall. It controls traffic to your EC2 instance base off the rules established for networking. Its the first line of defense.
  - Updates to security groups happen immediately.
  - Security groups are *stateful* Anything you add inbound will be allowed outbound.
  - Everything is blocked by default
  - All outbound traffic is allowed
  - Changes to security groups take effect immediately
  - You can have any number of EC2 instances within a security group
  - You can have multiple security groups attached to a EC2 instances
  - You _cannot block_ specific IP addresses using Security Groups, instead use Network Access Control List.
  - You can specify allow rules but not deny rules.
- **Volumes and Snapshots
  - Volumes exist on EBS
    - Virtual Hard Disk
  - Snapshots exist on S3.
  - `Snapshots` mare ppint in time copies of volumes.
  - Snapshots are incremental.  This means that only the blocks that have changed since your last snapshot are moved to s3
  - To create a snapshot for Amazon EBS volumes that serve as root devices, you should stop theinstance before taking the snapshot.
  - However you can take a snap while the instance is running
  - You can create AMIs from EBS=backed instances and snapshots.
  - You can change the EBS volume sizes on the fly, including changing the size and storage type.
  - **Volumes wil ALWAYS be in the ame availability zone as your EC2 instance**
  - To move an EC2 volume from one AZ/Region to another take a snao or an image of it, then copy it to the new AZ/Region
  - Snapshots of encrypted volumes ae encrypted automatically.
  - You can share snapshots, but only if they are unencrypted.
- For resolving package-lock.json in GIT.
```
just take theirs, `git checkout --theirs ./package-lock.json`
consider that resolved, add it, `git add ./package-lock.json`, and then just before commit, update it with `npm i`. then add it again if something changed `git add ./package-lock.json`
```
- AMI
  - Two Types
    - EBS
    - Instance Store
  - You can select AMI based on region, architecture, operating system, launch permissions and storage for the root device.
    - Instance Store (Ephemeral Storage)
    - EBS Backed Volumes
  - *EBS Volumes* The root device for an instance launched from the AMI is an Amazon EBS volume created from an Amazon EBS snapshot.
  - *Instance Store Volumes* THe root device for an instance launched from the AMI is an instance store volume created from a template stored in Amazon S3.
  - If you need faster provisioning time use EBS Backed volumes.
  - *Instance Store Volumes* are sometimes called Ephemeral Storage.
  - Instance store volumes **cannot** be stopped.  You will not lose the data on this instance if it is stopped.
  - EBS backed instances **can** be stopped. You will not lose the data on this instance if it is stopped.
  - By default, both ROOT volumes will be deleted on termination, however with EBS volumes you can tell AWS to keep the root device volume.
- **Elastic Load Balancer**
  - balance the load across your webservers.
  - Types of LB
    - `Application Load Balancers` are best suited for load balancing HTTP and HTTPS traffic.  They operate at LAyer 7 and are application aware.  They are intelligent, and you can create advanced request routing, sending specificied requests to specific webservers.
    - `Network Load Balancers` are best suited for load balancing of TCP traffic where extreme performance is required.  Operating at the connection level (Layer 4), Network Load Balancer are capable of handling millions of requets per second, while maintaining ultra-low latencies. Very fast.
    - `Classic Load Balancers` are teh legacy Elastic Load Balancers. Its old.  You can load balance HTTP/HTTPS applications and use Layer 7-specific features, such as X=Forwarded and sticky sessions.  You can also use strict Layer 4 load balancing for applications that rely purely on the TCP protocol.
  - `X-Forwarded-For` header can pass the public IP address to the EC2 instance.

#### 2019-02-20
- **ELB**
  - If you need the IPv4 address of your end user, look for the `X-Forwarded-For` header.
  - Instances monitored by ELB are reported as `InService` or `OutofService`
  - Healthchecks check the instance health bu talking to it
  - ELBs have their own DNS name, you are never given an IP address.
- **Cloudwatch**
  - `Dashboards` It creates dashboards to see what is happening with your AWS environment
  - `Alarms` allows you to set alarms that notify you when particular thresholds are hit.
  - `Events` cloudwatch events helps you respond to state changes in your AWS resources.
  - `Logs`  cloudwatch logs helps you aggregate, monitor, and store logs 
