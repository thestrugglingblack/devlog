#### 2019-03-19
- Test Coverage with Codecov or NYC, to make sure the test actually hit every line of code implemented.
  - `nyc --reporter=lcov tape ./test/initialize-ddb.test.js`
- I still need to develop a better understanding of differentiating between, functional, unit, integration and end-to-end test.
- theonlyblack.dev (ideas...animation....slideshow...comic..strip)
  - From a tech perspective, how do black tech professionals identify whether or not a team is inclusive?
  - I gauge the number of microaggressions I feel and pay attention to how they talk.If you give ppl enough time, they'll usually say _something_ that clues you into how they think.
  - And I look  at the current team dynamics. Are they talking a lot about inclusion but everyone is a white man or woman?
  - I also talk to the other Poc/Women in the team as well. you can normally see how inclusive they are by how much the people in that group have to hide themselves and who they are
  - if your company points to you as if *you* are the diversity, that is a red flag as well. They will never say this out loud, but by putting you in advertising material. or by always putting you in front and center to external people in one way or another
  - yea if you are always the front in center when they need a group picture, always have you meeting those when it comes to D&I initiatives
  - If they always talk about D&I but theres no actions behind the work. I.E. how do they handle recruiting and pipeline.  Also a big one for me is if they push the work of doing D&I initiatives on the minority groups who's job is not to implement these initiatives
  - also, be wary if they bring in poc and keep them at entry level positions.
- Look up on node `cluster module` and pm2

#### 2019-03-19
- 
