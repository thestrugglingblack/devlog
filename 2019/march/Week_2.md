#### 2019-03-13
- Need to fully understand the difference between
  - unit test
  - functional test
  - integration test
  - end to end test
- Need to work on getting the backend to connect to the following database types.
  - Postgres
  - MongoDB
  - MySQL

#### 2019-03-14
- **Software Architecture** is the process of converting software characteristics such as
    - flexibility
    - scalability
    - feasibility
    - reusability
    -  security
  into a structured solution that meets the _technical_ and the _business_ expectations.
- The software should be “**extendable, modular and maintainable**” if a business deals with urgent requests that need to be completed successfully in the matter of time.
- Software Architects would have to look into items such
  - low-fault tolerance
  - performance
  - scalability
  - reliability
  are the key characteristics.


- cluster module in node.js?? pm2 clusters allow for multiple event loops.
