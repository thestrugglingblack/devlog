### 09-04-2019
- **Terraform**
    - `provder` is a block used to configure the name provider.  A provider is responsible for creating and managing resources.  Multiple provider blocks can exist if a Terraform configuration is composed of multiple providers.
    ```
    provider "aws" {
        profile = "default"
        region = "us-east-1"
    } 
    ```
    - `resource` is a block that defines a resource that exists within the infrastructure.  A resource might be a physical component such as an EC@ instance or it can be a logical resource such as a Heroku application.
        - resource block has two strings before opening the block: `resource type` and `resource name`
     ```
     resource "aws_instance" "example" {
        ami = "ami-2757f631"
        instance_type = "t2.micro"
     ```
    - This was a credentials error. Had to configure to the default profile for `aws configure`.
    ```
    Error: error validating provider credentials: error calling sts:GetCallerIdentity: NoCredentialProviders: no valid providers in chain. Deprecated.
  	For verbose messaging see aws.Config.CredentialsChainVerboseErrors
    ```
    - Steps to apply configurations
        1. `terraform init` (Initialize)
        2.  `terraform apply` (Create resource)
        3.  `terraform show` (Show information about resource)
        
    - Example of what is shown
    ```
    Zuris-MBP:deploy zuri$ terraform show
    # aws_instance.raw_tweets_streamer:
    resource "aws_instance" "raw_tweets_streamer" {
        ami                          = "ami-2757f631"
        arn                          = "arn:aws:ec2:us-east-1:619419271872:instance/i-04685ec62dbe66334"
        associate_public_ip_address  = true
        availability_zone            = "us-east-1c"
        cpu_core_count               = 1
        cpu_threads_per_core         = 1
        disable_api_termination      = false
        ebs_optimized                = false
        get_password_data            = false
        id                           = "i-04685ec62dbe66334"
        instance_state               = "running"
        instance_type                = "t2.micro"
        ipv6_address_count           = 0
        ipv6_addresses               = []
        monitoring                   = false
        primary_network_interface_id = "eni-0b9fdafddc7517713"
        private_dns                  = "ip-172-31-41-122.ec2.internal"
        private_ip                   = "172.31.41.122"
        public_dns                   = "ec2-3-92-4-230.compute-1.amazonaws.com"
        public_ip                    = "3.92.4.230"
        security_groups              = [
            "default",
        ]
        source_dest_check            = true
        subnet_id                    = "subnet-cbf292bc"
        tenancy                      = "default"
        volume_tags                  = {}
        vpc_security_group_ids       = [
            "sg-71c75d15",
        ]
    
        credit_specification {
            cpu_credits = "standard"
        }
    
        root_block_device {
            delete_on_termination = true
            encrypted             = false
            iops                  = 100
            volume_id             = "vol-0b51bc040672ec920"
            volume_size           = 8
            volume_type           = "gp2"
        }
    }
    ```
  - When creating a resource with terraform.
    ```
    Zuris-MBP:deploy zuri$ terraform apply
    
    An execution plan has been generated and is shown below.
    Resource actions are indicated with the following symbols:
      + create
    
    Terraform will perform the following actions:
    
      # aws_instance.raw_tweets_streamer will be created
      + resource "aws_instance" "raw_tweets_streamer" {
          + ami                          = "ami-2757f631"
          + arn                          = (known after apply)
          + associate_public_ip_address  = (known after apply)
          + availability_zone            = (known after apply)
          + cpu_core_count               = (known after apply)
          + cpu_threads_per_core         = (known after apply)
          + get_password_data            = false
          + host_id                      = (known after apply)
          + id                           = (known after apply)
          + instance_state               = (known after apply)
          + instance_type                = "t2.micro"
          + ipv6_address_count           = (known after apply)
          + ipv6_addresses               = (known after apply)
          + key_name                     = (known after apply)
          + network_interface_id         = (known after apply)
          + password_data                = (known after apply)
          + placement_group              = (known after apply)
          + primary_network_interface_id = (known after apply)
          + private_dns                  = (known after apply)
          + private_ip                   = (known after apply)
          + public_dns                   = (known after apply)
          + public_ip                    = (known after apply)
          + security_groups              = (known after apply)
          + source_dest_check            = true
          + subnet_id                    = (known after apply)
          + tenancy                      = (known after apply)
          + volume_tags                  = (known after apply)
          + vpc_security_group_ids       = (known after apply)
    
          + ebs_block_device {
              + delete_on_termination = (known after apply)
              + device_name           = (known after apply)
              + encrypted             = (known after apply)
              + iops                  = (known after apply)
              + kms_key_id            = (known after apply)
              + snapshot_id           = (known after apply)
              + volume_id             = (known after apply)
              + volume_size           = (known after apply)
              + volume_type           = (known after apply)
            }
    
          + ephemeral_block_device {
              + device_name  = (known after apply)
              + no_device    = (known after apply)
              + virtual_name = (known after apply)
            }
    
          + network_interface {
              + delete_on_termination = (known after apply)
              + device_index          = (known after apply)
              + network_interface_id  = (known after apply)
            }
    
          + root_block_device {
              + delete_on_termination = (known after apply)
              + encrypted             = (known after apply)
              + iops                  = (known after apply)
              + kms_key_id            = (known after apply)
              + volume_id             = (known after apply)
              + volume_size           = (known after apply)
              + volume_type           = (known after apply)
            }
        }
    
    Plan: 1 to add, 0 to change, 0 to destroy.
    
    Do you want to perform these actions?
      Terraform will perform the actions described above.
      Only 'yes' will be accepted to approve.
    
      Enter a value: yes
    
    aws_instance.raw_tweets_streamer: Creating...
    aws_instance.raw_tweets_streamer: Still creating... [10s elapsed]
    aws_instance.raw_tweets_streamer: Still creating... [20s elapsed]
    aws_instance.raw_tweets_streamer: Still creating... [30s elapsed]
    aws_instance.raw_tweets_streamer: Creation complete after 31s [id=i-04685ec62dbe66334]
    
    Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
    ```
   - To remove resources run this command `terraform state rm [resource_type.resource_name]`
    ``` 
    Zuris-MBP:deploy zuri$ terraform state rm 'raw_tweets_streamer'
    
    Error: Invalid address
    
      on  line 1:
      (source code not available)
    
    Resource specification must include a resource type and name.
    
    Zuris-MBP:deploy zuri$ terraform state rm 'aws_instance.raw_tweets_streamer'
    Removed aws_instance.raw_tweets_streamer
    Successfully removed 1 resource instance(s).
    ```
  - Example of aws_instance terraform
  ```
    resource "aws_instance" "xray-01" {
      ami = "ami-ffffffffffffffffffff"
    
      instance_type        = "m5.xlarge"
      subnet_id            = "subnet-061d07843fffffffff"
      iam_instance_profile = "my.iam"
      key_name             = "backup.key"
      vpc_security_group_ids = ["sg-0defe3ffffffff"]
      count                = 1
    
      root_block_device {
        delete_on_termination = "true"
        volume_size           = 1000
        volume_type           = "gp2"
        encrypted             = "true"
        kms_key_id            = "<kms:arn>"
      }
    
      tags = {
        Name           = "xray-prod-01"
      }
  ```
- **AWS**
    - Connecting to EC2 Instance
    ```
    To access your instance:
    Open an SSH client. (find out how to connect using PuTTY)
    Locate your private key file (test-ec2.pem). The wizard automatically detects the key you used to launch the instance.
    Your key must not be publicly viewable for SSH to work. Use this command if needed:
    chmod 400 test-ec2.pem
    Connect to your instance using its Public DNS:
    ec2-54-218-123-133.us-west-2.compute.amazonaws.com
    Example:
    ssh -i "test-ec2.pem" ec2-user@ec2-54-218-123-133.us-west-2.compute.amazonaws.com
    Please note that in most cases the username above will be correct, however please ensure that you read your AMI usage instructions to ensure that the AMI owner has not changed the default AMI username.
    If you need any assistance connecting to your instance, please see our connection documentation.
    ```
  
  ## 09-05-2019
    - To generate pem files for EC2 instance [here](https://stackoverflow.com/a/32907112) 
    - My problems:
        - Dont know how to deploy an application to EC2.
        - Don't know how to deploy
    - Check out these articles for deploying to AWS
        - [Deploying a Flask Application  on AWS](https://medium.com/@rodkey/deploying-a-flask-application-on-aws-a72daba6bb80)
        - [Deploy Your Java Appliction with EC2](https://medium.com/@Sugeesh/deploy-your-java-application-with-ec2-bd70de34c4a0)
        - [Deplpoying a Go Application on AWS EC2](https://hackernoon.com/deploying-a-go-application-on-aws-ec2-76390c09c2c5)
        - [Deploying a MEAN App to Amazon EC2 Part 1](https://scotch.io/tutorials/deploying-a-mean-app-to-amazon-ec2-part-1)
        - [AWS EC2 Beginner Tutorial](https://www.datacamp.com/community/tutorials/aws-ec2-beginner-tutorial)
        - [Running a Flask App on AWS EC2](https://www.datasciencebytes.com/bytes/2015/02/24/running-a-flask-app-on-aws-ec2/)
    - To destroy a resource run `terraform destroy`
    -`ssh -i "raw_tweets_streamer_ec2.pem" ubuntu@ec2-35-170-79-0.compute-1.amazonaws.com -v` adding the "-v" makes the logs be very verbose afterwards.
    - `The source needs to be a CIDR block or a Security Group ID.` Don't really understand CIDR or Security Group ID is for an EC2 instance.
    - I was able to log into my instance but I had to change the `Security Group` rule for the `Inbound Traffic` I had to switch it to this. Add in a rule for SSH protocol. I had to change the Source to be my IP Address.
    _Make sure I update the IP Address_
    ```
    Type                    Protocol                Port Range                 Source                       Description
    All traffic             All                     All                        sg-71c75d15 (default)
    SSH                     TCP                     22                         69.255.192.169/32
    ``` 
  - Link for Troubleshooting [EC2 instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesConnecting.html#TroubleshootingInstancesConnectionTimeout)
  
  ## 09-06-2019
  - Maybe helpful for connecting pieces to apps with Terraform [Deploy a Clojure Web Application to AWS Using Terraform CircleCI](https://circleci.com/blog/deploy-a-clojure-web-application-to-aws-using-terraform/)
  
  ## 09-07-2019
  - *Packer*
    - basic commands are:
        - build: builds an image from template
        - console: creates a console for testing variable interoplation
        - fix: fixes templates from old versions of packer
        - inspect: see components of a template
        - validate: check that a template is valid
        - version: version of pcker
    - Basically can be used to create AMI images for AWS. For this project going to use it to create a base AMI image that have docker and all the stuff required for it to run the application.
    - `builder` is a components of Packer that is responsible for creating a machine and turning that machine into an image. 
- `ami-08c5925d121215084` the ami I created...
- To delete CUSTOM AMI
```
Updated answer from the aws docs:
In the navigation bar, verify your region.
In the navigation panel, click AMIs.
Select the AMI, click Actions, and then click Deregister. When prompted for confirmation, click Continue.
In the navigation pane, click Snapshots.
Select the snapshot, click Actions, and then click Delete.
```
- First initial script for installing docker on linux ubuntu 16
```bash
!/bin/sh

export DEBIAN_FRONTEND="noninteractive"

# Handle this error => The following signatures couldn't be verified because the public key is not available: NO_PUBKEY F76221572C52609D
sudo add-apt-repository ppa:webupd8team/y-ppa-manager
sudo apt-get update
sudo apt-get install y-ppa-manager

sudo apt update
sudo rm -rf /var/lib/apt/lists/*
sudo rm /var/cache/apt/*.bin

#VERSION-NAME=$(lsb_release -c)
#y=$(echo $VERSION-NAME | awk '{print $1}')

y=xenial
echo $y
cd /etc/apt/sources.list.d
touch docker_test.list
echo "deb https://apt.dockerproject.org/repo ubuntu-$y main" > docker_test.list

sudo apt-get install linux-image-extra-$(uname -r)
sudo apt-get update

sudo apt-get remove docker docker-engine

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common \
    dialog

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get install -y docker-ce

sudo groupadd docker
sudo usermod -aG docker ubuntu

sudo systemctl enable docker
```
- Another script for installing docker on Linux Ubuntu 16
```bash

#!/bin/bash

if [[ "$(whoami)" != "root" ]]
then
  echo "Switching to root..."
  exec sudo -E -- "$0" "$@"
fi

echo "Sleeping for 30 seconds..."
sleep 30

echo "Adding apt-key for get.docker.io..."
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 \
  --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9

echo "Setting up sources for get.docker.io..."
echo 'deb http://get.docker.io/ubuntu docker main' \
  > /etc/apt/sources.list.d/docker.list

export DEBIAN_FRONTEND=noninteractive

echo "Updating system..."
apt-get -y update
apt-get -y upgrade

echo "Installing kernel extensions..."
apt-get -y install linux-image-extra-$(uname -r)

echo "Installing lxc-docker-$version..."
apt-get -y install lxc-docker-$version

echo "Installing DOCKER"
sudo apt -y install docker.io

echo "Adding ubuntu user to docker group..."
usermod -aG docker ubuntu
```
- Example of me using Packer to create an AMI 
```json
{
  "builders": [
    {
      "access_key": "{{user `aws_access_key`}}",
      "ami_description": "{{user `us_east_1_name`}} AMI",
      "ami_name": "{{user `us_east_1_name`}} {{timestamp}}",
      "associate_public_ip_address": true,
      "instance_type": "t2.micro",
      "name": "{{user `us_east_1_name`}}",
      "region": "us-east-1",
      "run_tags": {
        "ami-create": "{{user `us_east_1_name`}}"
      },
      "secret_key": "{{user `aws_secret_key`}}",
      "source_ami": "{{user `us_east_1_ami`}}",
      "ssh_interface": "public_ip",
      "ssh_timeout": "10m",
      "ssh_username": "{{user `ssh_username`}}",
      "subnet_id": "",
      "tags": {
        "ami": "{{user `us_east_1_name`}}"
      },
      "type": "amazon-ebs",
      "vpc_id": ""
    }
  ],
  "provisioners": [
    {
      "destination": "/tmp/bootstrap_docker_ce.sh",
      "source": "bootstrap_docker_ce.sh",
      "type": "file"
    },
    {
      "destination": "/tmp/cleanup.sh",
      "source": "cleanup.sh",
      "type": "file"
    },
    {
      "execute_command": "echo 'packer' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'",
      "inline": [
        "whoami",
        "cd /tmp",
        "chmod +x bootstrap_docker_ce.sh",
        "chmod +x cleanup.sh",
        "ls -alh /tmp",
        "./bootstrap_docker_ce.sh",
        "sleep 10",
        "./cleanup.sh"
      ],
      "type": "shell"
    }
  ],
  "variables": {
    "aws_access_key": "",
    "aws_secret_key": "",
    "name": "aws-docker-ce-base",
    "ssh_username": "ubuntu",
    "us_east_1_ami": "ami-09b3691f",
    "us_east_1_name": "ubuntu-xenial-docker-ce-base"
  }
}
```
- [Tutorial](https://programmaticponderings.com/2017/03/06/baking-aws-ami-with-new-docker-ce-using-packer/#comments) I used to attemtpt to create an AMI Aws imge using Packer