#### 2019-01-08
- For viewing which container a particular container has a connection to (Network) go into the container file system and view the `etc/host` file to see what is listed.
- *Kubernetes*
  - `kubelets` runs pods
  - `kube proxy` exposes the containers that are on the pods to other nodes.
  - A `service` is basically a load-balancer.
  - `kubectl scale` command allows for the ability to scale up a a pods *replica* number.
  - `Pods` can only be exposed to the world be being connected to the defined services.

#### 2019-01-09
- `Node` is a worker machine
- `Deployment` is something that is responsible for creating and updating instances of your application
  - `kubectl run` to create a deployment
- `Pod` is a group of one ore more application containers that include shared volumes and IP addresses.
- `Service` is an abstraction layer which defines a logical set of Pods. Services will distribute traffic to newly deployed pods
  - `kubectl expose` to create a service
- The link between `Scaling` and `Deployment` is that the scale command just increase the number of ReplicaSets are in a deployment.
  - `kubectl run --replicas` to update the number of replicas
- Statuses for a ReplicaSets
  - *DESIRED*
  - *CURRENT*
  - *UP_TO_DATE*
  - *AVAILABLE*
- If a deployment was made available to the public and it as going through an update the network traffic is only sent to available instances (old or new)
  - `kubctl set image` to update a deployment
- `nomenclature` means the term or terms applied to someone or something.
  - i.e. `customers” was preferred to the original nomenclature “passengers`
