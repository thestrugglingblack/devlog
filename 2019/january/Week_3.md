#### 2019-01-14
- `.rc` stands for **run command**. So like `.bashrc`, `.npmrc` for examples. Its comes from Unix

#### 2019-01-15
- `.yaml` today I learned it stands for "Yet Another Markup Language."
- I don't really understand multitenancy
- [`knex.js`](https://knexjs.org/) is a "batteries included" SQL query builder for Postgres, MSSQL, MySQL, MariaDB, SQLite3, Oracle and Amazon Redshift designed to be flexible, portable and fun to use. This allows multiple hooks of DBs to an application that requires it.
- `Microservice Architectural Style` is an approach to developing a single application as a suite of small services, each running in its own process and communicatiing with lightweight mechanisms, often an HTTP resource API.

#### 2019-01-16
- For handling releases with some automation. Production and Beta releases should be done on the `master` branch and Dev releases be done on `develop` branch.
- *AWS SOLUTIONS ARCHITECT*
  - **Global Infrastructure**
    - 19 Regions
    - 57 Availability Zones
    - `region` is a geographical area consisting of two or more availability zones (AZ).
    - `availability zone` is one or more discrete data centers, each with redundant power, networking and connectivity, housed in separate facilities.
    - `edge locations` are endpoints for AWS which are used for caching content.(Cloudfront, Amazons Content Delivery Network)
    - 150 Edge Locations
  - **IAM**
    - `IAM` allows you to manage users and their level of access to the AWS console.
    - Features
      - Centralized control of AWS account
      - Shared access to your AWS  account
      - Granual Permissions
      - Identity Federator
      - Multifactor Authentication
      - Temporary Access to devices
      - Set up password rotation policy
      - Integrate with other AWs Services
      - _PCI DSS_ compliant
    - `Users` end users such as people employees of an organization
    - `Groups` a collection of users.  Each user in the group will inherit the permissions of the group
    - `Policies` are made up of documents...`Policy Documents`. `Policy Documents`  are in a format called JSON and they give permissions as to what User/Group/Role is able to do.
    - `Roles` create roles and assign them to AWS resources.
    - `Cloudwatch` is an AWS service used to monitor AWS resources and ser alarms.
    - **IAM is universal** its not locked down to a region.
    - The `root account` is simply the account created when first setup your AWS account. It has complete Admin access.
    - New Users have mo permissions when first created.
    - New Users are assigned **Access Key ID** and **Secret Access Keys** when created.
    - **QUESTIONS I AINT NOW**
      ```
      To save administration headaches, Amazon recommends that you leave all security groups in web facing subnets open on port 22 to 0.0.0.0/0 CIDR. That way, you can connect wherever you are in the world (THAT IS FALSE)

      You are a solutions architect working for a large engineering company who are moving from a legacy infrastructure to AWS. You have configured the company's first AWS account and you have set up IAM. Your company is based in Andorra, but there will be a small subsidiary operating out of South Korea, so that office will need its own AWS environment. Which of the following statements is true?
      ** You will need to configure Users and Policy Documents only once, as these are applied globally.

      When you create a new user, that user ________.
      ** Will be able to interact with AWS using their access key ID and secret access key using the API, CLI, or the AWS SDKs.

      Root Account have administrator access.

      You have a new member of staff who has started as a systems administrator, and she will need full access to the AWS console. You have created the user account and generated the access key id and the secret access key. You have moved this user into the group where the other administrators are, and you have provided the new user with their secret access key and their access key id. However, when she tries to log in to the AWS console, she cannot. Why might that be?
      ** You cannot log in to the AWS console using the Access Key ID / Secret Access Key pair. Instead, you must generate a password for the user, and supply the user with this password and your organization's unique AWS console login URL.

      ```
  - **S3**
    - `S3` is object-based. Allows you to upload files.
    - File can be from 0bytes to 5TB.
    - There is unlimited storage.
    - Files are stored in buckets.
    - S3 is a universal namespace.  That is, names must be unique globally.

#### 2019-01-17
- Today I learned about Paula Scher. She is the founder of Pentagram. A very famous graphic design company based out in New York City. Her company was recently the one to lead out the new design and logo for Slack.
