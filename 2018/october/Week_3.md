```
  sSSSs     sSSs. sss sssss   sSSSs   d ss.  d sss   d ss.
 S     S   S          S      S     S  S    b S       S    b
S       S S           S     S       S S    P S       S    P
S       S S           S     S       S S sSS' S sSSs  S sS'
S       S S           S     S       S S    b S       S   S
 S     S   S          S      S     S  S    P S       S    S
  "sss"     "sss'     P       "sss"   P `SS  P sSSss P    P

```
#### 2018-10-15
- Next presentation should be created through Impress.JS >>> https://impress.js.org/#/ing
- **Terraform**
  - TODO
    - https://blog.gruntwork.io/an-introduction-to-terraform-f17df9c6d180
    - https://blog.gruntwork.io/a-comprehensive-guide-to-terraform-b3d32832baca
- In my years of learning I didn't really look too much into testing and often when I discuss with others or do projects

#### 2018-10-19
- **Experience Mapping** “The activity of building an experience map builds knowledge and user understanding across the team and stakeholders and the map as an artifact allows designers to create and support seamless experiences through distinct phases of product/service.”
    - Resources
        - https://blog.prototypr.io/the-art-of-experience-mapping-complete-process-515f9ed9da04
        - http://adaptivepath.org/ideas/our-guide-to-experience-mapping/
- Look into this for **GitFlow** >> https://danielkummer.github.io/git-flow-cheatsheet/#hotfixes
- JS Concepts....probably should do a blog on what I learned https://github.com/leonardomso/33-js-concepts
- **.DS_Store** is a file that stores custom attributes of its containing folder, such as the position of icons or the choice of a background image.
- **FastCGI** is a binary protocol for interfacing interactive programs with a web server. FastCGI is a variation on the earlier Common Gateway Interface (CGI); FastCGI's main aim is to reduce the overhead associated with interfacing the web server and CGI programs, allowing a server to handle more web page requests per same amount of time.
- **Mantissa** the part of a floating-point number that represents the significant digits of that number, and that is multiplied by the base raised to the exponent to give the actual value of the number
- Difference between `value types` and `reference types`.
- Difference between `assign values` and `assigning pointers`.
- Typings
    - Implicit
    - Explicit
    - Nominal
    - Structural
    - Duck
- Scope
    - Function
    - Lexical
    - Block
- `Message Queues` and `Event Looping`
- The science behind `setInterval`, `requestAnimationFrame`, and `setTimeout`
- What is `jspref` and `performance.now` do or tell us when it comes to javascript performance?
- What are `opts` and `deopts`?
- `Call Stack` is a to-do list of function invocations.
