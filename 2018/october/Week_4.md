```
  sSSSs     sSSs. sss sssss   sSSSs   d ss.  d sss   d ss.
 S     S   S          S      S     S  S    b S       S    b
S       S S           S     S       S S    P S       S    P
S       S S           S     S       S S sSS' S sSSs  S sS'
S       S S           S     S       S S    b S       S   S
 S     S   S          S      S     S  S    P S       S    S
  "sss"     "sss'     P       "sss"   P `SS  P sSSss P    P

```
#### 2018-10-20
- Javascript can only do one thing at a time. It is basically **single-threaded**. There is some magic that can bring it out of single threading which is **thread pooling**.
- Last In, First Out Structure...thats what a **Stack**.

#### 2018-10-23
- For adding a global gitignore file >>  https://help.github.com/articles/ignoring-files/#create-a-global-gitignore
- The `Event Loop` is a **queue** (First In, First Out) and the `Call Stack` is a **stack**. (https://gist.github.com/jesstelford/9a35d20a2aa044df8bf241e00d7bc2d0)
- A `Starved Event Loop` is when code running in the current `Call Stack` can prevent code on the `Event Loop` from being **executed**.
- Understand the difference between an expression and a statement, and what it means to evaluate an expression.
- Understand `IIFE's`, `modules`, and `namespaces`.
- Represent numbers in the following:
  - Binary
  - Hex
  - Dec
  - Scientific Notation
- What are ?
  - `Bitwise Operators`
  - `Typed Arrays`
  - `Array Buffers`
- Manipulate `RGBA` like manipulating binary data.

#### 2018-10-24
- https://fossa.io
- **TLS: Transport Layer Security**
  - It is a protocol that provides privacy and data integrity between two communication applications.
  - Evolved from Netscape's Secure Socket Layer protocol (SSL)

#### 2018-10-26
- **Error Handling in JS**
  - Is async/await suppose to be wrapper in front of a promise?
  - (https://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/)
```
// to.js
export default function to(promise) {
   return promise.then(data => {
      return [null, data];
   })
   .catch(err => [err]);
}
```
