```
  sSSSs     sSSs. sss sssss   sSSSs   d ss.  d sss   d ss.
 S     S   S          S      S     S  S    b S       S    b
S       S S           S     S       S S    P S       S    P
S       S S           S     S       S S sSS' S sSSs  S sS'
S       S S           S     S       S S    b S       S   S
 S     S   S          S      S     S  S    P S       S    S
  "sss"     "sss'     P       "sss"   P `SS  P sSSss P    P

```
#### 2018-10-02
- **The League**
  - Figure out how to use OmniGraffle
  - Sketch architecture
  - Figure how to deploy a private npm package
      ```
      sequelize deprecated String based operators are now deprecated. Please use Symbol based operators for better security, read more at http://docs.sequelizejs.com/manual/tutorial/querying.html#operators node_modules/sequelize/lib/sequelize.js:242:13
      Unhandled rejection SequelizeConnectionError: Client does not support authentication protocol requested by server; consider upgrading MySQL client
      ```
  - This error happened after making this request in Postman
      ```
      POST /user/new/ HTTP/1.1
      Host: localhost:8889
      Content-Type: application/json
      Cache-Control: no-cache
      Postman-Token: 019ac10b-9a54-1ff9-c212-680a1c22b57f

      {
       "firstName": "Jalissa",
       "lastName": "Doe",
       "emailAddress": "jdoe@gmail.com"
      }
      ```
  - At first I thought I didn't properly initiate the User model so I added this within the Sequelize file.
      ```
      const User = UserModel(sequelize, Sequelize);

      module.exports = {
        User
      }
      ```
  - But it looks like I need to do something with mysql server locally on my machine.
  - Implemented the wait-for-it.sh script so that the database starts before it connects. I am faced with a new error
  ```
  server_1  | Wed, 03 Oct 2018 03:32:41 GMT sequelize deprecated String based operators are now deprecated. Please use Symbol based operators for better security, read more at http://docs.sequelizejs.com/manual/tutorial/querying.html#operators at node_modules/sequelize/lib/sequelize.js:242:13
  server_1  | Unhandled rejection SequelizeConnectionRefusedError: connect ECONNREFUSED 127.0.0.1:3306
  ```
  - I think I needed to switch the name to `db` so it would be http://db:3306 due to docker network level.
  - Below how you would store data in a volume within docker-compose
  ```
  volumes:
   - ./data/mysql:/var/lib/mysql
   ```

#### 2018-10-04
- **Terraform**
  - it is a tool used to create, manage, and update infrastructure resources such as physical machines, VMS, netowrk switches, containers and more.
  - `provider` is responsible for understanding API interactions and exposing resources. Providers generally are IaaS (AWS, Azure) and SaaS (Cloudflare, DNSimple) They are the ones that actually give you actual access to the resource. Basically plugins.
  - `resources` are the services that the providers offer so for example AWS is the provider and its resources would be ELB, DDB, S3, EC2s.

  - creating a SSH key with no passphrase makes it easy for terraform to not be prompted to answer a passphrase
  - `/usr/local/bin` I had to move the terraform executable just so the commands can be recognize
  - Run `terraform init` to initialize repo for terraform. Actually creates a .terraform folder with configurations.
  -`terraform plan -var "do_token=${DO_TOKEN}" -var "pub_key=$HOME/.ssh/id_ecdsa.pub" -var "pvt_key=$HOME/.ssh/id_ecdsa" -var "ssh_fingerprint=$SSH_FINGERPRINT"` in the `provider.tf`  those variables are defined but empty this command passes those values in. That command then creates the resource which is basically a nginx webserver. This is the output:
  ```
  An execution plan has been generated and is shown below.
  Resource actions are indicated with the following symbols:
    + create

  Terraform will perform the following actions:

    + digitalocean_droplet.web1
        id:                   <computed>
        backups:              "false"
        disk:                 <computed>
        image:                "ubuntu-16-04-x64"
        ipv4_address:         <computed>
        ipv4_address_private: <computed>
        ipv6:                 "false"
        ipv6_address:         <computed>
        ipv6_address_private: <computed>
        locked:               <computed>
        memory:               <computed>
        monitoring:           "false"
        name:                 "web1"
        price_hourly:         <computed>
        price_monthly:        <computed>
        private_networking:   "true"
        region:               "nyc2"
        resize_disk:          "true"
        size:                 "512mb"
        ssh_keys.#:           "1"
        ssh_keys.2066252898:  "3b:3f:58:9f:51:ec:3d:8e:2b:e1:d7:1e:a1:ba:1c:af"
        status:               <computed>
        vcpus:                <computed>
        volume_ids.#:         <computed>


  Plan: 1 to add, 0 to change, 0 to destroy.

  ------------------------------------------------------------------------

  Note: You didn't specify an "-out" parameter to save this plan, so Terraform
  can't guarantee that exactly these actions will be performed if
  "terraform apply" is subsequently run.


  ```
- Follows declaration configuration.
- Looks like `plan` is showing what configuration is going to look like. And `apply` is basically creating the resource.
- `* digitalocean_droplet.web1: Error creating droplet: POST https://api.digitalocean.com/v2/droplets: 422 Region is not available
` error came up when trying to run the `apply` command. It looks like the flavor image is not available in the `nyc2`. Changed image to 14 and moved region to `nyc1`
- It looks like every time I either run `apply` or `plan` it creates `terraform.tfstate` file and a `terraform.tfstate.backup`
