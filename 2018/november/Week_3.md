```
.-----.-----.--.--.-----.--------.|  |--.-----.----.
|     |  _  |  |  |  -__|        ||  _  |  -__|   _|
|__|__|_____|\___/|_____|__|__|__||_____|_____|__|  
```
#### 2018-10-13
- Learned about this site for keeping a changelog > https://keepachangelog.com/en/1.0.0/

#### 2018-10-14
- **Testing**
  - `e2e` test are usually associated with front-end
  - `unit` test are testing each small component of a functionality. Mainly functionas that are exported from the module.
  - `integration` test are test that accounts for the full usage of its functionality. Mainly capturing the end result of it.
#### 2018-10-15
- `axios` does not work with `nock` for mocking requests
- `sinon` maybe a good option but I can't figure if I need to use mock or stub. I will say with stubbing out the request it was still not intercepting the function and still was kicking off the real function. I wonder if its the way how I have axios > request.js > routes/get-scores.js, like the level abstractness that is happening with that. Will give it a shot tomorrow.
- Look at https://github.com/mapbox/atlas-installer/blob/master/test/download.test.js for understanding how to stub requests with `sinon`

#### 2018-10-16
- [Google SRE Book](https://landing.google.com/sre/sre-book/toc/)
- The later half of this tutorial may be useful for Kubernetes (https://hackernoon.com/containerizing-a-node-js-api-using-docker-with-kubernetes-and-minikube-30255fd33ef9)
- RGB colors for Web and CMYK colors for Print Illustrations
- `Flow.js` is for type checking. I guess I need to explore why type-checking is super important in these loosely typed languages (ruby, python and etc)  The only thing I can think of is that we would want to implement typechecking is if a error occurs in production because something didn't receive the correct data type.
- [JMeter](http://jmeter.apache.org/usermanual/get-started.html#running) a load testing tool to explore performance of a service.
- A Guide to Troubleshooting the Performance of a Service
  - Follow the traces of where it fails (Logging Tool)
  - Take into account which infrastructure it is in (Cloud vs. On-Premise)
  - Explore how memory is being handled (Cache or CPU Memory)
  - Calculate processing time of the business logic/request/data access
  - Eliminate any external service or vendor software that can contribute to speed performance issue
    ```
    Elliot Fisher >>
    Ooo girl, you talking my language now. Step one is having the ability to turn on excessive logging. Troubleshooting performance is all about having the data. You need to know how long every action takes and be able to correlate each log with a request, so adding some kind of correlation ID that's generated automatically on each request added to the log. Once you know where the issue is you can start to figure out where to clear up the bottleneck. 250 requests is nothing so it should be pretty obvious where the issue is. You can use something like JMeter to simulate load on the endpoint

    Johnny Ray
    i'd say try and isolate where the problem is first. is your server that's slow or is it that you're deployed in us-west-1 and the bulk of traffic is coming from the east coast? are you caching heavily enough? is it the business logic that's taking a while or is it because you're hitting a database? are you talking to any external services which could be a bottleneck?

    ```
  -
