```
.-----.-----.--.--.-----.--------.|  |--.-----.----.
|     |  _  |  |  |  -__|        ||  _  |  -__|   _|
|__|__|_____|\___/|_____|__|__|__||_____|_____|__|  
```
#### 2018-10-05
- [Handling Git Submodules Effectively](https://www.philosophicalhacker.com/post/using-git-submodules-effectively/)
  - Submodules are apparently tricky but its like any new framework or concept being introduced.
- How do we handle **version control** in deployments?
  - HELM....now everything makes sense.
- **Helm**
  - `chart` defines a group of manifest files.
- Looks like I needed to start `minikube start` to spin up the cluster in order to apply the namespace.
- To add support of ES2015 and ES2017 Syntax
  - Install these dependencies
    - babel-cli
    - babel-polgyfill
    - babel-plugin-transform-async-to-generator
    - babel-preset-2017
    - babel-preset-2015
  - Create `.babelrc` file at the root of the directory
  ```
  {
    "presets": ["es2015","es2017"],
    "plugins": "transform-async-to-generator"
   }

  ```

#### 2018-10-06
- Turns out `async/await` is only available in node version 8 and above with out having to add in the babel transpiler and stuff. All service is suppose to be on the latest version of node.

#### 2018-10-07
- For Docker and disabling iptables. Mac doesn't have iptables so to test those items you will have to go through
- **docker-compose** make default networking by going after the name of the directory of where the docker-compose file is located.
```
See https://docs.docker.com/compose/networking/: "Your app’s network is given a name based on the “project name”, which is based on the name of the directory it lives in. You can override the project name with either the --project-name flag or the COMPOSE_PROJECT_NAME environment variable."
```

#### 2019-10-08
- So to analyze a moment when messages come back at 500x or 400x setting up a debug nginx server to record the logs can be an avenue to explore. It appears to be a lot for small projects but it seems ideal in large architectures that receive high volumes of traffic.  To limit a debug-nginx server setting it with `max_conn` directive will limit the number of concurrent connections. (https://www.nginx.com/blog/capturing-5xx-errors-debug-server/)

```
upstream debug_server {
    server 172.16.0.9 max_conns=20;
}
```
- `Docker and Healthchecks`
```
healthcheck:
  test: ["CMD", "curl", "-f", "http://localhost"]
  interval: 1m30s
  timeout: 10s
  retries: 3
  start_period: 40s
restart: unless-stopped
```
So if you start the full service and you stop one of the services that have the `restart` policy set to `unless-stopped` it will continue to try to restart itself.
- `Nginx` for intercepting error status codes use the `proxy_interceptor_error` directive and `error_page` directive.
```
location /some-path {
        proxy_pass http://some-service;
        proxy_intercept_errors on;

        error_page 500 502 /404.html;
    }
```
