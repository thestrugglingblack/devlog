```
.-----.-----.--.--.-----.--------.|  |--.-----.----.
|     |  _  |  |  |  -__|        ||  _  |  -__|   _|
|__|__|_____|\___/|_____|__|__|__||_____|_____|__|
```
#### 2018-11-25
- Creating custom error page in Nginx
```
error_page 400 404 405 =200 @40*_json;

location @40*_json {
  default_type application/json;
  return 200 '{"code":"1", "message": "Not Found"}';
}

error_page 500 502 503 504 =200 @50*_json;

location @50*_json {
  default_type application/json;
  return 200 '{"code":"1", "message": "Unknown Error"}';
}
```
-`In-Memory Cache` is a way of reducing input/output an application needs to do for data stored elsewhere (redis, s3, DynamoDB, files, other APIs)
  - Reasons to use in-memory cache
    - **To reduce latency** if data is stored in-memory, its very fast to access compared to connecting to another service and transferring data.
    - **To reduce load on other services** even if speed is not an issue, an in-memory cache can reduce the number of reads an application needs to do to another service.
- `Node-Locking` (https://github.com/mapbox/locking) for handling in-memory cache for services
```
var Locking = require('@mapbox/locking');
var fs = require('fs');
var readFile = Locking(fs.readFile);

// Reads file once, calls callback 10x.
for (var i = 0; i < 10; i++) {
    readFile('./sample.txt', function(err, data) {
        console.log(data);
    });
}
```
- `Container Network Model (CNM)`
  - **Sandbox** contains the configuration of a container's network stack
  - **Endpoint** joins a Sandbox to a Network
  - **Network** a group of Endpoints that are able to communicate with each-other directly. Network consists of many endpoints.
- Documentation on `Libnetwork` (https://github.com/docker/libnetwork/blob/master/docs/design.md)
  - Portability and Extensibility are the two main factors in this project. It is a driver interface. Features are:
    - builtin IP address management
    - multihost networking
    - Service discovery and load balancing
    - Plugins to extend the ecosystems
- `Container Network Interface` is another standard for container networking which was started by coreOS and is used by CloudFoundry, Kubernetes.
- **docker network**
  ```
  Usage:	docker network COMMAND

  Manage networks

  Commands:
    connect     Connect a container to a network
    create      Create a network
    disconnect  Disconnect a container from a network
    inspect     Display detailed information on one or more networks
    ls          List networks
    prune       Remove all unused networks
    rm          Remove one or more networks
  ```
  - The `None Networking` mode offers a container-specific network stack which does not have external network interface.  It only  has a local loopback interface.
- `Tasks` are scheduled so the browser can get from its internals into JS/DOM land and ensures these actions happen sequentially.  Between tasks, the browser may render updates.  Getting from a mouse click to an event callback requires a task, as does parsing, HTML and `setTimeout`.
- `Microtasks` are usually schedules for things that should happen straight after the currently executing script, such as reacting to a batch of actions, or to make something async without take the penality of a whole new task. The microtask queue is processed after callbacks as long as no other JS is mid-execution, and at the end of each task.  Any additional microtask queued during microtasks are added to the end of the queue and also processed.  Microtasks include mutation observer callbacks, and as in the above example, `promise callbacks`.
- `Promise callbacks are queued as microtasks` (depends on the browser)
- Tasks executes in order and the browser may render between them.
- Microtasks execute in order and are executed:
  - after every callback, as long as no other JS is mid-execution
  - at the end of each task
- **Transport Layer Security (TLS)**
  - Composed of two layers: the TLS Record and the TLS Handshake Protocol.
    - `The Record Protocol` provides connection security.
    - `The Handshake Protcol` allows the server and client to authenticte each other and to negotiate encryption algorithms and cryptographic keys before any data is exchanged.
  - TLS and SSL are not _interoperable_, though TLS currently provides some backward compatibility for legacy systems.
  - TLS is not vulernable to the POODLE attack because it specifies that all padding bytes must have the same value and be verified.
- **Node.js: module.exports vs exports**
  - `module.exports` is basically the variable that get returned from `require()`..it is an empty object by default.
  ```
  exports.someMethod = function( ) {}
  ```
  - `exports` is never returned
  ```
  module.exports.someMethod = function () {}
  ```
  - `module.exports` internally wraps are required functions in a function wrapper.
  - Variable **module** is object representing the current module and it is local to each module and private.
  - To use `module.exports`:
    - **Attaching public methods** to it.
    - **Replacing it** with out custom object or function.
      - Replacing it allows for an arbitrary instance of some other class
      ```
      module.exports = class Calculator {
          add(a,b) { return a + b }
          substract(a,b) { return a - b }
        }
      ```
#### 2018-11-26
- [Kubernetes Containerd Integration Goes GA](https://kubernetes.io/blog/2018/05/24/kubernetes-containerd-integration-goes-ga/) REVIEW

#### 2018-11-28
- Dumb shit in Javascript `.splice` >> https://stackoverflow.com/a/7517997
  - it returns an empty array if there are no items being removed...It just modify the original array.
  ```
  var a = [1,2,3,4,5];
  var b = a.splice(1, 0, 'foo');
  console.log(a);   // [1,'foo',2,3,4,5]
  console.log(b);   // []
  ```
- **React and Flow**
  - `propTypes` are a way to make it strict on data types for `this.props`, to prevent breakage amongst other components.  Basically trying to imitate Jave and other strict type languages but putting it in React.js
  - `Flow.js` is are more expressive way of the *propTypes* utility but with the advantage of having a IDE and CLI tool to catch things immediately when everyting is out of wack.
    - A `?` in front of a type declaration means the type can be null.
    ```
    type Props = {
      foo: number,
      bar?: string,
    };
    ```
- `sed` is suppose to read the file and change the output of it as it streams.

#### 2018-11-30
- For `cherry-pick` commits into a new branch >> https://stackoverflow.com/a/45440646 (RANGE)
  ```
  # new branch
  git cherry-pick COMMIT_FROM_ORIGIN..COMMIT_FROM_ORIGIN
  ```
- **Kubernetes Up and Running**
  - `Service Discovery`
    -
