```
.-----.-----.--.--.-----.--------.|  |--.-----.----.
|     |  _  |  |  |  -__|        ||  _  |  -__|   _|
|__|__|_____|\___/|_____|__|__|__||_____|_____|__|  
```
#### 2018-11-01
- Kubernetes is a platform used to organize baby computers. Containers are the babies and Kubernetes is the parent.
- Virtual Machine is a way to virtualize another OS within your machine.
- Containers are easy to use to reproduce your infrastructure.
- Containers are going to need support for organizing amongst their selves so this would include having some form of management on networking, scheduling, communication with other containers and etc.
- A `container` is a linux thread.
- `Containers` are easier to transfer around, theres resource isolation and is able to encompass application environments. Also a great way to abstract your infrastructure.
- The way how Kubernetes comes into play is through answering these questions?
  - **Where do my containers live?**
    - Scheduling
  - **How do I talk to other containers?**
    - Networking
  - **How do I talk to the world?**
    - Routing
  - **What happens if I get sick and die?**
    - Failure Recovery
- Some containers can be tightly coupled together. This means `that some containers require another container in order to work`. This can reveal the container dependency tree.  A prime example of that is node.js api container needing to spin up at the same time as its database.  The `Kubernetes Scheduler` will come into play and tell where the container should live with.
- `Pods` are basically scheduling units. They contain more than one containers. Each pod is given a IP address within a cluster so it can be reached. This IPAddress can not be reached outside of the cluster.
- `Deployments` are a group of pods.
- `ReplicaSets` specify the number of **pods** that can run in your cluster.
- `Controller (Deployment Controller)` will always make sure you have the desired number of pods spun up base off of looking at the `ReplicaSet` specifications.
- Kubernetes `Services` deals with a group of pods or deployments.  An example of a Kubernetes `Service` is a service A is actually Back-End and service B is actually Front-End. You need both services to be able to talk to each other because thats how they work with each other.
- `Ingress Controller` helps route traffic and endpoint from the world into cluster.
- `Deployments` are in charged of making sure that everything is in a desired state.
- `Node JS Clinic Doctor`
  - https://github.com/nearform/node-clinic
  - https://clinicjs.org
- `Lab for Playing with Kubernetes` >> https://labs.play-with-k8s.com/
- Everything is a thumbroll, everything dies, things a complicated and network goes in and out.  The idea is build trust in the service, meaning you want to access your application with low latency and make sure that everything work.
- What is helm?
  - Kubernetes package manager. However I am unsure on what type of packages its managing.
- **Redis** is a database that can be used precisely for caching and speeding up your application.
  - Written in ++
  - Key<>Value Store database which means
  -
- **Cloud Guru Kubernetes Deep Dive**
  - Control Plane (Brains of Kuberenetes) and Worker Nodes (Apps run under it)
