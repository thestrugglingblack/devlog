#### 2018-09-17
- Awesome personal website by [Cat Small](http://cattsmall.com/about/)
  - She has great tutorials for game development.
- **Stream Processing**
  - `Keyed Aggregation` is taking a unique ID of the data and then formulating some logic or calculations on that set of information.
    - Example: Take a userID of a Twitter account and doing an analysis on how many keywords it has used in regards to "Blockchain"
  - `Non-Keyed Global Aggregation` is taking a key that isn't unqiue to a specific reford and formulating some logic or calcuation on that set of information.
    - Example: Pulling in data on storm reports and only aggregating on a set of fields that have category 5 storms.
    - This type of aggregation is great for events such as `Complex Even Proccessing` (CEP) applications.  CEPs seatch for complex patterns in the data stream. This type of application will need to see the entire data set to detect a pattern.
  - `Windowing`  defines how records are selected from the stream and grouped together to a meaningful frame.
    - "Defining Windows"
      - `Tumbling Window` divides the continuous stream into discrete parts that don't overlap. Defined by a `time duration`(time-based window) or a `record count`(count-based window)
      - `Time-Based Sliding Window`is a fixed size window, but consecutive windows can be overlapped. Defined size and sliding step.
      - `Session Window` is a burst of user activity followed by a period of inactivity(timeout). IT collects the activity beloinging to the saem session. Doesn't have a fixed start or duration...It is `data-driven`.
    - A stream record preesents an event that happened in the real world and has its own timestamp (called `event time`), which can be quite different from the moment the record is processed (called `procssing time`)
      - Reasons of this:
        - Records could be delayed on its way from source to stream processing system.
        - Link speed from various sources may vary.
        - Original device may be offline when event occurs.
      - If items are sensitive it is best interest to accurately calculate
    - `Event Skew` is the difference between processing time and event time.
    ```
    The system needs to be instructed on how long to wait for delays. If delays are long, there is a high chance of capturing all of the information but if delays are short there is a high chance of faster responses (lower latency) and few resource consumption. However the loss is there is less data.
    ```
    - `Watermarks` are heuristic algorithms that help assess window completeness.
  - `Jobs`
    - **Jobs** are a units of work or units of execution that performs an ultimate action.  A component of a jon is called a `task` or `step`.  A job can be identified with a single process, which may in turn have `subprocesses` or `child processes`
    ```
    #EXAMPLE

    A simple example of a job stream is a system to print payroll checks which might consist of the following steps, performed on a batch of inputs:

    1. Read a file of data containing employee id numbers and hours worked for the current pay period (batch of input data). Validate the data to check that the employee numbers are valid and that the hours worked are reasonable.

    2. Compute salary and deductions for the current pay period based on hours input and pay rate and deductions from the employee's master record. Update the employee master "year-to-date" figures and create a file of records containing information to be used in the following steps.

    3. Print payroll checks using the data created in the previous step.

    4. Update bank account balance to reflect check numbers and amounts written.

    Each step depends on successful completion of the previous step. For example, if incorrect data is input to the first step the job might terminate without executing the subsequent steps to allow the payroll department to correct the data and rerun the edit. If there are no errors the job will run to completion with no manual intervention.
    ```
    - When to use `Client-Server`
      - Cluster shared for multiple jobs
      - Isolating client application from the cluster
    - When to use `Embedded`
      - When you want simplicity
      - Microservices
      - SPE is embedded in your application.

#### 2018-09-19
- **Stream Processing**
  - `Fault Tolerance`
    - `Exactly-Once Processing Guarantee` is when the original input items are stored until the results of their processing have been emitted...after it resumes after failure, the items are processed again. This accounts for every item being captured.  This is achievable but it cost siginificantly when it comes to latency, throughput and storage overhead.
    - `At-least Processing Guarantee` is when the system may end up replaying an item that was already processed.
  - `Sources and Sinks`
    - Streams Processing access **sources** and **sinks** through `connectors`.

#### 2018-09-20
- `Parallelism` is isolated. Today I finally understand concurrency.
