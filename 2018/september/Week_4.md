#### 2018-09-24
- To change to remote url of local codebase `git remote set-url LINK_TO_REPO`
- **Kubernetes Up and Running**
  - `Resource Management`
    - Resource management is measured in:
      - `Request Limits` for the minimum amount of a resource required to run the application.
      - `Resource Limits` for the maximum amount of a resource that an application can consume.
    - Resources are requested per container, not per Pod.`The total resources requested by the Pod is the sum of all resources requested by all containers in the Pod`
    - Requests are used when scheduling Pods. The `Kubernetes Scheduler` will ensure that the sum of all requests of all Pods on a node does not exceed the capacity of the node.
    - CPU requests are implemented using the `cpu-shares` functionality in the Linux kernel.
    ```
      Consequently when the system runs out of memory, the KUBELET terminates the containers whose memory usage is greater than their requested memory.
    ```
    - Example of `Request  Limit` in kubernetes yaml
    ```
    spec:
      containers:
        - image: gcr.io/kuar-demo/kuard-amd64:1
          name: kuard
          resources:
            requests:
              cpu: "500m"
              memory: "128mi"
            limits:
              cpu: "1000m"
              memory: "256Mi"
    ```
    _When you establish limits on a container, the kernel is configured to ensure that consumption cannot exceed these limits_
  - **Volumes Pods**
    - Add `spec.volumes` (Its an array)
    - Add `volumeMounts`(Its an array)
      - This defines the volumes that are mounted into a particular container and the path where each volume should be mounted.
    - Example of adding Volumes
    ```
    spec:
      volumes:
        - name: "kuard-data"
          hostpath:
            path: "/var/lib/kuard"
      containers:
        - image: gcr.io/kuar-demo/kuard-amd64:1
          name: kuard
          volumeMounts:
            - mountPath: "/data"
            name: "kuard-data"
    ```
    - **Ways to use Volumes with Pods**
      - Communication/Synchronization
        - To achieve this, the Pod uses an `emptyDir` volume. Such volume is scoped to the Pod's lifespan, but it can be shared between two containers.
      - Cache
        - For instances like serving thumbnails its best to use a cache, so that it can survive a container restart due to a healthcheck failure, and thus `emptyDir` works well for the cache use case.
      - Persistent data
        - Persistent data is data that is `independent` of the lifespan of a particular Pod., and should move between nodes in the cluster if a node fails or a Pod moves to a different machine for some reason.
      - Mounting Host Filesystem
        - Kubernetes support `hostDir` volume, which can mount arbitrary locations on the worker node into the container.
    - Kubernetes automatically mounts and unmounts the appropriate storage whenever a Pod using that volume is scheduled onto a particular machine.
    - Example of a NFS server yaml.
    ```
    volumes:
      - name: "kuard-data"
        nfs:
          server: my.nfs.server.local
          path: "/exports"
    ```
  - **Labels**
    - `Labels` are key/value pairs that can be attached to Kubernetes objects such as Pods and ReplicaSets.  They can be arbitrary and are useful for attaching identifying information to Kubernetes objects.  Labels provide the foundation for grouping objects.
    - `Kubernetes uses labels to deal with sets of objects instead of instances`
    - You can use `-L` option to `kubectl get` tol show label value as a column.
    - To remove a lbale by applying a dash suffix like this >> `kubectl label deployments alpaca-test "canary-"`
    - `Label Selectors` are used to filter Kubernetes objets based on a set of labels.
    - There maybe a label called `pod-template-hash`.  This label is applied by the deployment so it can keep track of which pods were generated from which template versions.
    - To look at list of pods with a `ver` label with a value of 2 use the `--selector` flag like this >>> `kubectl get pods --selector="ver=2"`
    - For search on two labels run this >>> `kubectl get pods --selector="app=bandicot,ver=2"`
    - For a label with one set of value run this >> `kubectl get pods ---selector="app in (alpaca,bandicoot)"`
    - For to see if any particular labelis set run this >> `kubectl get deployments --selector="canary"`
    - `Labels are used to identify and group objects`
  - **Annotations**
    - `Annotations` are key/value pairs designed to hold nonidentifying information that can be leveraged by tools and libraries.
    - It provides a place to store additional metadata for Kubernetes objects with the sole purpose of assisting tools and libraries.  It can be used for the tool itself or to pass configuration information between external systems.
    - `Annotations ared used to provide extra information about where an object came from, how to use it or policy around that object`
    - It used for:
      - Keep track of reason for latest update to object.
      - Communicate a specialized scheduling policy to a specialized scheduler
      - Extend data about the last tool to update the resource and how it was updated. (Used for detections and smart merge)
      - Build, release, or image information that isnt appropriate for labels (i.e. Git hash, timestamp, PR number)
      - Enabled the `Deployment` object to keep track of `ReplicaSets` that it is managing for rollouts.
      - Provide extra data to enhance the visual quality or usability of UI.
- **Service Discovery**
  - Domain Name System (DNS) is the traditional system of service discovery on the internet.  DNS is designed for relatively stable name resolution with wide and efficient caching.
  - A `Service` object is a way to create a named label selector. Use `kubectl expose` to create a service. >> `kubectl expose deployment alpaca-prod`
  - The kubernetes service is automatically created for you so that you can find and talk to the kubernetes API from within the app.
  - When created the services is assigned a new `virtual IP` address called a `cluster IP`. This is a special IP address the system will load balance across all of the pods that are identified by the selector.
  - **Service DNS**
    - The `cluster IP` is virtual it is stable and it is appropriate to give it a DNS address.
    - An example of a DNS name is `alpaca-prod.default.svc.cluster.local`

#### 2019-09-25
- **Stream Processing**
  - `Connectors` job is to handle computation work and serve as the point of contact with the outside world.
  - **Items Connectors Need to Beware of**
    - Unbounded? or Bounded?
      - bound(finite) or unbounded(infinite) data.
      - `Bounded Data` is handled in batch jobs and there are fewer concerns to deal with, as data boundaries are within the dataset itself. There is no need to worry about windowin, late events, or event time skew. Examples are plain files, HDFS, or iterating over database query results.
      - `Unbounded Data` streams allows for continuous processing; however, you have to use windowing for operations that cannot effectively work on infinite. (Popular choice is Kafka)
    - It is Replayable?
      - This question is important for making sure you can achieve fault tolerance. If anything goes wrong during the computation, the data can be replayed and the computation can be restarted from the beginning.
      - Replayability of infinite data sources is limited by the disk space necessary to store the whole data stream.
      - Example of resources that are only `read-only`are TCP socket source and JMS Queue sourcr.
      - Implement checkpoints within data streams (so you don't play from the very beginning) `Checkpointing` is the ability of the stream sourcr to replay its dta from the point (offset) you choose.
    - Is it Distributed?
      - If data is distributed connectors should be operate as a distributed system.
    - Data Locality?
      - meaning how quick can your data be available and accessible.
  - **List of Stream Processing Platform**
    - Apache Flink
    - Apache Storm
    - Google Cloud Data Flow
    - Hazelcast Jet
    - Kafka Streams
    - Spark Streaming
  - [Resource Link](https://dzone.com/storage/assets/9223150-dzone-refcard265-streamprocessing-0523.pdf)
- **AWS Comprehend**
  - **Kinesis Stream**
    - A `shard` is a unit of throughput capcity. Each shard ingests up to 1MB/sec and 1000 records/sec, and emits up to 2MB/sec.  To accomodate for a higher or lower throughput, the number of shards can be modified after the kinesis stream is created using the API. The best analogy is shards are equivalent to lanes on a highway. The more lanes on a highway the fast traffic can flow.
  [Data Processing Diagram]('../../diagrams/data-processing.png')
  - In this example Kinesis Firehose is acting as an intermediary before sending the data off to destination. It has the process of allowing a `transformation`, this process would be called `ETL` short for `Extract Transform Load`.
  [Processing Data Overview in Kinesis Firehouse]('../../diagrams/processing-data.png')

  #### 2018-09-27
  - Learned about "Disney 12 Principles in Animation"  this can possibly be applied to Web Design and 3D Modeling/Animation (https://www.creativebloq.com/advice/understand-the-12-principles-of-animation)

  #### 2018-09-30
  - Angular 2-5 ....don't know which version it is on.
  - Angular CLI
    - Create a new angular app `ng new NAME_OF_APP`
    - To run the app `ng serve` within the application directory.
    - To create a component `ng generate component NAME_OF_COMPONENT`
