#### 2018-09-05
- [Torque Game Enginer](http://www.garagegames.com/)
- **Docker Monitoring**
  - Two ways of Monitoring Data (VM World)
    - Ask developers to instrument their code directly and push datat to a central collection point
    - Leverage a transparent form of push-based instrumentation to see all application and container activity on the host.
  - Monitoring Tools
    - Docker Stats API
    - cAdvisor
    - Prometheus
    - Sysdig
  - **Docker Stats API**
    - `docker ps` list containers running.
    - `/stats/` gives a longer list of information for the containers.
      ```
      #EXAMPLE OF A CALL
      curl --unix-socket /var/run/docker.socknhttp:/containers/8a9973a456b3/stats?stream=false
      ```
  - **cAdvisor**
    - Simple server that taps the Docker API and provides one minute of historical data in one-second incrememnts
    - Need to specify which directory to checkout
  - **Prometheus**
    - use in conjunction with a visual UI dashboard like grafana. Requires maintainence of a back-end. Doesn't cover CPU, memory or network data.
  - **Sysdig**
    - Can be installed as a container
    - `-pc` or `-pcontainer` allows for you to view the requesting containers context.
    - `topprocs_cpu` is a **chisel** (built in commands or services from sysdig) command that lets you see the top processes in terms of CPU usage.
      - `$ sudo sysdig -pc -c topprocs\_cpu`
    - Command that shows all of the processes from a WordPress
    `sudo sysdig -pc -c topprocs\_cpu container.name contains wordpress`
    - Command to view network utilizations broken up by processes
    `sudo sysdig -pc -c topprocs\_net\`
    - Command to view top connections
    `sudo sysdig -pc -c topconns\`

#### 2018-09-06
- **Stream Processing**
  - Streaming Systems goals is to process big data volumes and provide userful insights into the data prior to saving it to a long-term storage.
  - Streamg Processing goal is to overcome latency, an issue that comes with the batching apporach. It processes the live, raw data immediately as it arrives and meets the challenge of incremental processing, scalability and fault tolerance.
    - `batching` is where data is collected for an entire day and then during down time an offline batch job kicks off to process the data.
  - Process is common with real-time analytics, detecting anomalies, fraud or pattern detection, complex event processing, real-time stats, real-time extract transodrm load and implementing event-driven architectures.
  - Should be aimed to make the consumption more consistent and predictable.
  - A `stream` is a sequence of records. Each records holds information about an event.
  - Records are `immutable`
  - Stream processing ingests more than one stream sources and to process that data it goes through what we call `transformations`.

#### 2018-09-07
- `Directed Acyclic Graph`
  - Graph a structure of nodes, that are connected to each other with edges.
  - Directed, the connections between the nodes (edges) have a direction. Example A => B is not the same as B => A
  - Acyclic, means non-circular, moving from note to node by following the edges, where you will never encounter the same node for the second time.
    - Data Structure a `Tree`
    ```
    Example:
    I see lot of answers indicating the meaning of DAG (Directed Acyclic Graph) but no answers on its applications. Here is a very simple one -

    Pre-requisite graph - During an engineering course every student faces a task of choosing subjects that follows requirements such as pre-requisites. Now its clear that you cannot take a class on Artificial Intelligence[B] without a pre requisite course on Algorithms[A]. Hence B depends on A or in better terms A has an edge directed to B. So in order to reach Node B you have to visit Node A. It will soon be clear that after adding all the subjects with its pre-requisites into a graph, it will turn out to be a Directed Acyclic Graph.
    ```
- **Stream Processing**
  - `Source Connectors` are used to connect the stream processing application to the system that feeds the data.
    - Apache Kafka
    - Confluent
    - JMS Broker
  - Steps to build a Stream Processing Application
    - Define the transformations
    - Connect the streaming application to stream resources and sinks.
    - Define the dataflow of the application by wiring the sources to `transformations` and `sinks`.
    - Excute the streaming processing application using hte stream processing engine.
      ```
      The engine takes care of passing the records between the system components (sources, processors and sinks) and invoking them when the record arrives.
      ```
  - `Transformations` are used to express the business logic of a streaming application. On the low level, a processing task receives some stream items, performs arbitrary processing, and emits some items. It may emit items even without receiving anything (acting as a `stream source`) or it may just receive and not emit anything (acting as a `sink`)
    - Requires **declarative** process. Stream Processing share similiar principles with functional and dataflow programming.
  - *Types of Stateless Transformations*
    - `Map` transforms one record to one record, example is hange format of the record, enrich record with some data.
