#### 2018-27-12
- **Design is Storytelling**
  - `Action`
    - Key points on Action
    - Tools
      - *Narrative Arc* is where you divide a story into 5 phases: `exposition`, `rising action`, `climax`, `falling action` and `conclusion/denouement`.
