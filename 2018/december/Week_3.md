#### 2018-12-19
- *Continuous Integration* is a software development priactice in which you build and test software every time a developer pushes code tot he application.
- *Continuous Delivery* is a software engineering approach in which continuous integration, automated testing and automated deployment capabilities allow software to be developed and deployed rapidly, reliably and repeatedly with minimal human intervention.  The deployment to production is defined strategically and triggered manually.
- *Continuous Deployment* is a software development practice in which every code changes goes through the entire pipeline and is put into production automatically, resulting in many production deployments every day.  It does everything that _Continuous Delivery_ does, but the process is fully automated, theres no human intervention at all.

#### 2018-12-20
- `Call Stack` is a mechansim for an interpreter to keep track of its place in a script that calls multiple functions -- what function is currently being run, what functions are called from within that function and should be called next.
```
* When a script calls a function the interpreter adds it to the call stack and then starts carrying out the function.

* Any function that are called by that function are added to the cal stack further up, and run where their calls are reached.

* When the current function is finished, the interpreter takes it off the stack and resumes execution where it left off in the last code listing.

* If the stack takes up more space than it had assigned to it, it results in  a "stack overflow" error.
```
-
