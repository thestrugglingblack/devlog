#### 2018-12-04
- I need to understand why this is happening
```
# GET /status
(node:20774) UnhandledPromiseRejectionWarning: Error: There was problem with the request, here is the following error:
 Error: Request failed with status code 404
    at request (/Users/zurihunter/Documents/misc/football-service/helpers/request.js:9:11)
    at <anonymous>
    at process._tickCallback (internal/process/next_tick.js:188:7)
(node:20774) UnhandledPromiseRejectionWarning: Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). (rejection id: 2)
(node:20774) [DEP0018] DeprecationWarning: Unhandled promise rejections are deprecated. In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.
```
The function is async/await function wrapped in a try/catch block and its still not picking up the unhandled promise rejection.


#### 2018-12-05
- http://nginx.org/en/docs/http/ngx_http_core_module.html#location
`@someLocation { }`
```
The “@” prefix defines a named location. Such a location is not used for a regular request processing, but instead used for request redirection. They cannot be nested, and cannot contain nested locations.
```

#### 2018-12-06
- To save my ass in git >> https://sethrobertson.github.io/GitFixUm/fixup.html
- High Level Curriculum in Design (https://mackenziechild.podia.com/design-for-developers)
  - Foundation of Design
  - Design Mistakes to Avoid
  - Planning Projects
  - Developing Ideas and Finding Inspiration
  - Theory & Psychology of Color
  - Principles & Anatomy of Typography
  - Working with Photography
  - User Experience
- Activities to Accomplish in Design Curriculum
  - Write a Design Brief
  - Create a Mood Board & Style Guide
  - Choose Colors and Fonts
  - Create Userflow Wireframes
  - Create Low & Mid-fidelty Wireframes
  - Design Desktop & Mobile UI
- Japanese Anime Animation Vroid (https://vroid.pixiv.net/download.html)

#### 2018-12-07
- Great Interview Resource for Questions (Fullstack Cafe)>> https://www.fullstack.cafe/JavaScript
- For the-league application in testing the request.js helper its an async/await method....So when testing it I had to await for the data to come back this goes the same for testing it out when it errors.....
