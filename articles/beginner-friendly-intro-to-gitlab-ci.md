The goal of this tutorial is to give a high-level introduction to using Gitlab CI and avoid going down the rabbit hole of reading the documentation.  This tutorial is geared toward beginners who wish to get tinker with CI/CD tools like Gitlab CI but are way too lazy to piece together the documentation. In this tutorial, I will briefly go over what exactly CI/CD is in the software development lifecycle, why I  decided to go with Gitlab tool and provide walk you through how to create a `.gitlab-ci.yml` with an example application.

### CI/CD
CI/CD short for **Continuous Integration/ Continuous Delivery / Continuous Deployment**. It is a practice that allows for teams to build, test and release software at a faster rate.  The idea is that this process would have very minimal human interaction and everything automated except the part where it comes to manually deploy the code to production. One of the challenges of implementing this practice is integrating the tools and technologies to create this automation.  For example, the codebase can live on bitbucket, automated testing conducted on private testing suite and several components of the application can be deployed to AWS and Microsoft Azure.  Due to some applications complex setup, it has contributed to not all organization implementing a seamless CI/CD pipeline.

### Why Gitlab CI?
The reason I decided to dive into Gitlab CI was that it was the first CI/CD tool that I came across that kept everything in one location, ran through the pipeline fast and it is open source. With Gitlab CI in the same location, I can create tickets, merge requests, write code and setup CI/CD tools without going to another application window. It is essentially a one-stop shop. Gitlab runs its build off these tool called `Runners`.  Runners are isolated virtual machines that run predefined steps through the Gitlab CI API.  This tool alone allows for projects to run through the pipeline builds faster compared to running on a single instance. And finally, it is open source so I can always contribute to the code base and I place in a ticket when a problem has arisen.

### Scenario
Let's say we have a Node.js API that retrieves a list of books in a database.  What we want to do is create a pipeline that pushes our code through a "build, test and deploy" phase. With that description we can break down our pipeline into three types:
* Project Pipeline
* Continuous Integration Pipeline
* Deploy Pipeline

In the "Project Pipeline" we will have a series of steps that will install dependencies, run linters and any miscellaneous items dealing with the code. Then in the "CI Pipeline" it has steps focused on running automated tests and compiling the code using PKG or some Node.js compilation tool. Finally, it will end with the "Deploy Pipeline" section that will deploy the code to a designated cloud provider and environment.

A *pipeline* is a group of steps that are grouped by similar characteristics. The steps are called *jobs*. When you group a series of jobs by those characteristics it is called *stages*.  Jobs are the basic building block, they can be grouped together in stages, and stages can be grouped together in pipelines. If we were to map out the hierarchy of this it will look like the following:

```
A.) Build
     i. Install NPM Dependencies
     ii. Run ES-Linter
     iii. Run Code-Minifier
B.) Test
     i. Run unit, functional and e2e test.
     ii. Run PKG to compile Node.js application
C.) Deploy
     i. Production
        1.) Launch EC2 instance on AWS
     ii. Staging
        1.) Launch on local development server
```

In this hierarchy, all three components are considered three different pipelines. The `build, test and deploy` are *stages* and each bullet under those sections are jobs. Let's break this out into GitlabCI .yaml file.

### Using Gitlab CI
To use Gitlab CI, create a file called `.gitlab-ci.yml` at the root of the project and add this following:

```
image: node:10.5.0

stages:
  - build
  - test
  - deploy

before_script:
  - npm install
```
GitlabCI uses containers (Runners) to run the builds so the `image` directive allows for us to specify what type of OS/libraries we want our pipeline to run off on and in this case we are doing this to our Node API. The `stages` directive allows us to predefine stage throughout the entire configuration. The order of the stages is the way how the jobs associated with those stages are going to be executed. View here for more about stages (https://docs.gitlab.com/ee/ci/yaml/#stages) The `before_script` directive is used to run a command before all jobs.

Now let's start with our job dedicated to the `Build` stage. We are going to call this job `build-min-code`.  In this job we want it to install dependencies and minify the code. We can start this off with using the `script` directive. The `script` directive is a shell script that gets executed within the Runner. Then we the job to its stage. To assign a job to a stage, use the `stage` directive.

```
image: node:10.5.0

stages:
  - build
  - test
  - deploy

before_script:
  - npm install

build-min-code
stage: build
script:
  - npm install
  - npm run minifier
```

Now we have a job associated with our `Build` stage and we are going to do that for our `Test` stage. Our test job is going to be called `run-unit-test` and we are going to utilize the npm script in our API to run a test `npm test`
```
image: node:10.5.0

stages:
  - build
  - test
  - deploy

before_script:
  - npm install

build-min-code
stage: build
script:
  - npm install
  - npm run minifier

run-unit-test
stage: test
script:
  - npm run test
```
Finally, we are going to add a job to handle our `Deploy` stage: `deploy-production`, `deploy-staging`. In this instance, we are going to have two different jobs for deployment (staging and production). These jobs will reflect the same layout as our previous jobs but with a small change. Currently, all of our jobs will be triggered on any code push. We do not want to have that for when we deploy our code to staging and production.  To prevent that from happening we use the `only` directive. The `only` directive defines the names of branches and tags for which the job will run.  The job will look like the following:

```
image: node:10.5.0

stages:
  - build
  - test
  - deploy

before_script:
  - npm install

build-min-code
stage: build
script:
  - npm install
  - npm run minifier

run-unit-test
stage: test
script:
  - npm run test
  -
deploy-staging:
  stage: deploy
  script:
    - npm run deploy-stage
  only:
    - develop

deploy-production:
  stage: deploy
  script:
    - npm run deploy-prod
  only:
    - master
```
In our `deploy-staging` job, the Runner will only execute it if there was a change to the `develop` branch and for `deploy-production` the `master` branch.

### Beyond Basic GitlabCI
Now we have Gitlab continuous integration implemented
